#!/bin/sh
curl -L http://geolite.maxmind.com/download/geoip/database/GeoLite2-City.mmdb.gz > GeoLite2-City.mmdb.gz
gzip -df GeoLite2-City.mmdb.gz

curl -L http://geolite.maxmind.com/download/geoip/database/GeoLite2-Country.mmdb.gz > GeoLite2-Country.mmdb.gz
gzip -df GeoLite2-Country.mmdb.gz
